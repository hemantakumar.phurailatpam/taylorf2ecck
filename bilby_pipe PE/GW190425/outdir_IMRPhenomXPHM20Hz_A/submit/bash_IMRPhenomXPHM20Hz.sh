#!/usr/bin/env bash

# IMRPhenomXPHM20Hz_data0_1240215503-017_generation
# PARENTS 
# CHILDREN IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_IMRPhenomXPHM20Hz_A/IMRPhenomXPHM20Hz_config_complete.ini --label IMRPhenomXPHM20Hz_data0_1240215503-017_generation --idx 0 --trigger-time 1240215503.017

# IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1
# PARENTS IMRPhenomXPHM20Hz_data0_1240215503-017_generation
# CHILDREN IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_IMRPhenomXPHM20Hz_A/IMRPhenomXPHM20Hz_config_complete.ini --outdir outdir_IMRPhenomXPHM20Hz_A --detectors L1 --detectors V1 --label IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1 --data-dump-file outdir_IMRPhenomXPHM20Hz_A/data/IMRPhenomXPHM20Hz_data0_1240215503-017_generation_data_dump.pickle --sampler dynesty

# IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1_final_result
# PARENTS IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_IMRPhenomXPHM20Hz_A/result/IMRPhenomXPHM20Hz_data0_1240215503-017_analysis_L1V1_result.hdf5 --outdir outdir_IMRPhenomXPHM20Hz_A/final_result --extension hdf5 --max-samples 20000 --lightweight --save

