#!/usr/bin/env bash

# TaylorF2EccFD3PN_data0_1187008882-43_generation
# PARENTS 
# CHILDREN TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_TaylorF2EccFD3PN/TaylorF2EccFD3PN_config_complete.ini --label TaylorF2EccFD3PN_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1
# PARENTS TaylorF2EccFD3PN_data0_1187008882-43_generation
# CHILDREN TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_TaylorF2EccFD3PN/TaylorF2EccFD3PN_config_complete.ini --outdir outdir_TaylorF2EccFD3PN --detectors H1 --detectors L1 --detectors V1 --label TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_TaylorF2EccFD3PN/data/TaylorF2EccFD3PN_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_TaylorF2EccFD3PN/result/TaylorF2EccFD3PN_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_TaylorF2EccFD3PN/final_result --extension hdf5 --max-samples 20000 --lightweight --save

