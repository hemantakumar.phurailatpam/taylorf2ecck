#!/usr/bin/env bash

# PerAdvFDLeO_e0_logPrior_data0_1187008882-43_generation
# PARENTS 
# CHILDREN PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_PerAdvFDLeO_e0_logPrior/PerAdvFDLeO_e0_logPrior_config_complete.ini --label PerAdvFDLeO_e0_logPrior_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1
# PARENTS PerAdvFDLeO_e0_logPrior_data0_1187008882-43_generation
# CHILDREN PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_PerAdvFDLeO_e0_logPrior/PerAdvFDLeO_e0_logPrior_config_complete.ini --outdir outdir_PerAdvFDLeO_e0_logPrior --detectors H1 --detectors L1 --detectors V1 --label PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_PerAdvFDLeO_e0_logPrior/data/PerAdvFDLeO_e0_logPrior_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_PerAdvFDLeO_e0_logPrior/result/PerAdvFDLeO_e0_logPrior_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_PerAdvFDLeO_e0_logPrior/final_result --extension hdf5 --max-samples 20000 --lightweight --save

