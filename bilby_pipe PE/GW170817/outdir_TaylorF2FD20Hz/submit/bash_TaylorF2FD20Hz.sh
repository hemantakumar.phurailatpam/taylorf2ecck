#!/usr/bin/env bash

# TaylorF2FD20Hz_data0_1187008882-43_generation
# PARENTS 
# CHILDREN TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_TaylorF2FD20Hz/TaylorF2FD20Hz_config_complete.ini --label TaylorF2FD20Hz_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1
# PARENTS TaylorF2FD20Hz_data0_1187008882-43_generation
# CHILDREN TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_TaylorF2FD20Hz/TaylorF2FD20Hz_config_complete.ini --outdir outdir_TaylorF2FD20Hz --detectors H1 --detectors L1 --detectors V1 --label TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_TaylorF2FD20Hz/data/TaylorF2FD20Hz_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_TaylorF2FD20Hz/result/TaylorF2FD20Hz_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_TaylorF2FD20Hz/final_result --extension hdf5 --max-samples 20000 --lightweight --save

