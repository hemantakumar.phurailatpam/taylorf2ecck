#!/usr/bin/env bash

# PerAdvFDCir_e0_Uniform_data0_1187008882-43_generation
# PARENTS 
# CHILDREN PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_PerAdvFDCir_e0_Uniform/PerAdvFDCir_e0_Uniform_config_complete.ini --label PerAdvFDCir_e0_Uniform_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1
# PARENTS PerAdvFDCir_e0_Uniform_data0_1187008882-43_generation
# CHILDREN PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_PerAdvFDCir_e0_Uniform/PerAdvFDCir_e0_Uniform_config_complete.ini --outdir outdir_PerAdvFDCir_e0_Uniform --detectors H1 --detectors L1 --detectors V1 --label PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_PerAdvFDCir_e0_Uniform/data/PerAdvFDCir_e0_Uniform_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_PerAdvFDCir_e0_Uniform/result/PerAdvFDCir_e0_Uniform_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_PerAdvFDCir_e0_Uniform/final_result --extension hdf5 --max-samples 20000 --lightweight --save

