#!/usr/bin/env bash

# TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_generation
# PARENTS 
# CHILDREN TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_TaylorF2EccFD20Hz_e0fixed/TaylorF2EccFD20Hz_e0fixed_config_complete.ini --label TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1
# PARENTS TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_generation
# CHILDREN TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_TaylorF2EccFD20Hz_e0fixed/TaylorF2EccFD20Hz_e0fixed_config_complete.ini --outdir outdir_TaylorF2EccFD20Hz_e0fixed --detectors H1 --detectors L1 --detectors V1 --label TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_TaylorF2EccFD20Hz_e0fixed/data/TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_TaylorF2EccFD20Hz_e0fixed/result/TaylorF2EccFD20Hz_e0fixed_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_TaylorF2EccFD20Hz_e0fixed/final_result --extension hdf5 --max-samples 20000 --lightweight --save

