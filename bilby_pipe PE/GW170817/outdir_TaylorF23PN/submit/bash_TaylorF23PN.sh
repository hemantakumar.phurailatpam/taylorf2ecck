#!/usr/bin/env bash

# TaylorF23PN_data0_1187008882-43_generation
# PARENTS 
# CHILDREN TaylorF23PN_data0_1187008882-43_analysis_H1L1V1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_TaylorF23PN/TaylorF23PN_config_complete.ini --label TaylorF23PN_data0_1187008882-43_generation --idx 0 --trigger-time 1187008882.43

# TaylorF23PN_data0_1187008882-43_analysis_H1L1V1
# PARENTS TaylorF23PN_data0_1187008882-43_generation
# CHILDREN TaylorF23PN_data0_1187008882-43_analysis_H1L1V1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_TaylorF23PN/TaylorF23PN_config_complete.ini --outdir outdir_TaylorF23PN --detectors H1 --detectors L1 --detectors V1 --label TaylorF23PN_data0_1187008882-43_analysis_H1L1V1 --data-dump-file outdir_TaylorF23PN/data/TaylorF23PN_data0_1187008882-43_generation_data_dump.pickle --sampler dynesty

# TaylorF23PN_data0_1187008882-43_analysis_H1L1V1_final_result
# PARENTS TaylorF23PN_data0_1187008882-43_analysis_H1L1V1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_TaylorF23PN/result/TaylorF23PN_data0_1187008882-43_analysis_H1L1V1_result.hdf5 --outdir outdir_TaylorF23PN/final_result --extension hdf5 --max-samples 20000 --lightweight --save

