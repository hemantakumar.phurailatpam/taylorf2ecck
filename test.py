import numpy as np
import bilby
import pickle

injection_parameters = dict(
    mass_1=36., mass_2=29., a_1=0.0, a_2=0.0, tilt_1=0.0, tilt_2=0.0,
    phi_12=0., phi_jl=0., luminosity_distance=4000., theta_jn=0.4, psi=2.659,
    phase=1.3, geocent_time=1126259642.413, ra=1.375, dec=-1.2108)

waveform_arguments = dict(waveform_approximant='EccentricTD',
                          reference_frequency=50., minimum_frequency=20.)
duration = 4.
sampling_frequency = 2048.
geocent_time=1249852257.0

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    time_domain_source_model=bilby.gw.source.lal_binary_black_hole,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    waveform_arguments=waveform_arguments)

test = waveform_generator.frequency_domain_strain(parameters=injection_parameters)

with open('filename.pickle', 'wb') as handle:
    pickle.dump(test, handle, protocol=pickle.HIGHEST_PROTOCOL)
