# GCC compilable code

## link gcc to lal first with the correct path
$ export C_INCLUDE_PATH="/home/albert.einstein/lalsuite-install/include:$C_INCLUDE_PATH"
* lalsuite-install is the dir where you have installed the lalsuite with lalsimulation

## run the code
* $ gcc LALSimInspiralTaylorF2Ecch.c -lm -ldl -l lal
* or
* $ gcc LALSimInspiralTaylorF2Ecck.c -lm -ldl -l lal

## mathematica files
* related mathematica files can be found in the dir: waveform_mathematica

