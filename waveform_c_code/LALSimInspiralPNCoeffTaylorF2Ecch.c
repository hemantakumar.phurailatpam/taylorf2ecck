/*
 *  Copyright (C) 2022 Phurailatpam Hemantakumar
 *  Assembled from code found in:
 *    - LALSimInspiralTaylorF2Ecc.c
 *    - LALSimInspiralEccentricityFD.c
 */

/*
 * TaylorF2Ecch
 * For 3 harmonics and leading order in et0 and no periastron advancement effect
 * no spin
 * no tidal
 */

// #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <lal/LALConstants.h>
#include <lal/LALAtomicDatatypes.h>

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

// sine cosine values needed for calculatng harmonics.
// s2b stands for sin(2*beta), and ci_2 stands for pow(cos(iota), 2) .
typedef struct
{
    REAL8 ci, si_2, ci_2;
} sincos_;

// xi is the harmonics dependent amplitude. p and c stands for plus and cross polarization respectively.
typedef struct
{
    COMPLEX16 xi_p, xi_c;
} xi_;

// chi = f/f_min
// chi power values. p stands for power.
// these values are use for calculating the PN coefficients of psi (fourier phase).
typedef struct
{
    REAL8 p1, p19b9, p25b9, p28b9, p31b9, p34b9, p37b9, p19b18;
} chi_struct;

////////////////////////////////////
// early declaration of functions //
////////////////////////////////////
// function to calculate the harmonics dependent part of the amplitude, xi
void Xi_PlusCrossYunes( size_t cc, REAL8 et, sincos_* sc, COMPLEX16 *xi_p, COMPLEX16 *xi_c); /*function for calculating xi (harmonics dependent amplitude) */
// function to calculate PN coefficients for psi (fourier phase).
void psiPNe0Coeff(REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void psiPNe0CoeffHM( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 ln_x, REAL8 f, REAL8 flso ); /* Harmonic indices dependent */

////////////////////////////////////////////////////////////////
// function to find xi : harmonic dependent part of amplitude //
////////////////////////////////////////////////////////////////
void Xi_PlusCrossYunes( size_t cc, REAL8 et, sincos_* sc, COMPLEX16 *xi_p, COMPLEX16 *xi_c)
{
    // et : 0PN leading order eccentricity at frequenc f
    // sc : sine cosine functions of inclination angle
    // xi_p : for plus polarization
    // xi_c : for cross polarization
    cc+=1 ;
    switch( cc )
    {            
        // l = 1, n = -2
        case 1:
        {   
            *xi_p = et*(1.5 + 1.5*sc->ci_2 + sc->si_2);
            *xi_c = et*(3.*I*sc->ci);
            break;
        }
        // l = 2, n = -2
        case 2:
        {
            *xi_p = - 2. - 2.*sc->ci_2;
            *xi_c = - 4.*I*sc->ci;
            break;
        }
        // l = 3, n = -2
        case 3:
        {
            *xi_p = - et*(4.5 + 4.5*sc->ci_2);
            *xi_c = - et*(9.*I*sc->ci);
            break;
        }

        default:
        {
            *xi_p = 0.*I;
            *xi_c = 0.*I;
            break;
        }
    }
}


///////////////////////////////////////////
// function to calculate PN coefficients //
///////////////////////////////////////////
// needed psi (fourier phase).
// Harmonic indices independent
void psiPNe0Coeff(REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    // chi : f/f_min
    // symmetric mass ratio
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    
    //////////////////////////
    // psi 0PN Coefficients //
    //////////////////////////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 
    
    ////////////////////////////
    // psi 3b2PN Coefficients //
    ////////////////////////////
    se[2] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[3] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */
    
    //////////////////////////
    // psi 1PN Coefficients //
    //////////////////////////
    se[4] = 4.914021164021164 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[5] = -5.876726535208678/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */
    
    //////////////////////////
    // psi 2PN Coefficients //
    //////////////////////////
    se[6] = 30.103152950995213 + 53.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[7] = (- 7.8535738913553095 - 40.0192436558364*eta - 36.277272048441404*eta_2)/chi->p19b9 + (- 16.516633208577563 + 3.1391372239586524*eta + 56.50271923709424*eta_2)/chi->p25b9 + (0.6305695963516759 + 11.775120739510275*eta - 22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */
    
    ////////////////////////////
    // psi 5b2PN Coefficients //
    ////////////////////////////
    se[8] = 160.59106891266873 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[9] = (121.02483595536498 + 216.02141641228727*eta)/chi->p19b9 + (141.88004620884573 - 276.2479120677702*eta)/chi->p25b9 + (- 96.67055421018408 - 169.84953027662291*eta)/chi->p28b9 + (- 42.66622732728404 + 211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */
    
    //////////////////////////
    // psi 3PN Coefficients //
    //////////////////////////
    se[10] = 173.81123372926686 - 3308.3205476758876*eta + 44.01331018518518*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[11] = (- 263.56525867551164 - 533.6543962376188*eta - 135.68214874177875*eta_2 - 92.75010314857681*eta_3 )/chi->p19b9 + (- 22.072594081557135 - 69.4982179486792*eta + 117.036345528134*eta_2 + 198.5172942650821*eta_3)/chi->p25b9 + 830.4127436366826/chi->p28b9 + (2.3005144737812744 + 47.00130032318599*eta - 6.844822374723789*eta_2 - 144.6428183515857*eta_3)/chi->p31b9 + (- 416.7706682875983 + 28.006148883847995*eta + 7.068382176074206 + 35.5644754417582*eta_3)/chi->p37b9; /* psi3PN_et0_2_Coeff */

}

///////////////////////////////////////////
// function to calculate PN coefficients //
///////////////////////////////////////////
// needed for psi (fourier phase).
// Harmonic indices dependent
void psiPNe0CoeffHM( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 ln_x, REAL8 f, REAL8 flso )
{
    // chi : f/f_min
    // ll : harmonic index
    // ln_x : log of PN parameter
    // f : frequency 
    // flso : frequency at last stable orbit
    // symmetric mass ratio
    REAL8 eta_2 = eta*eta;

    //////////////////////////
    // psi 1PN Coefficients //
    //////////////////////////
    se[4] = 0.; /* psi1PN_et0_0_Coeff */

    se[5] = 0.; /* psi1PN_et0_2_Coeff */

    //////////////////////////
    // psi 2PN Coefficients //
    //////////////////////////
    se[6] = 0.; /* psi2PN_et0_0_Coeff */

    se[7] = 0.; /* psi2PN_et0_2_Coeff */

    ////////////////////////////
    // psi 5b2PN Coefficients //
    ////////////////////////////
    se[8] =   (160.59106891266873 - 22.689280275926283*eta)*log(f/flso) + (- 160.59106891266873 + 22.689280275926283*eta)*log(ll); /* psi5b2PN_et0_0_Coeff */

    se[9] = 0.; /* psi5b2PN_et0_2_Coeff */

    //////////////////////////
    // psi 3PN Coefficients //
    //////////////////////////
    se[10] = - 452.0639897594774 - 163.04761904761904*ln_x; /* psi3PN_et0_0_Coeff */
    

    se[11] = (-20.04978125656214 - 21.8553869047619*ln_x)/chi->p19b9 + (89.30841235662422 + 21.202568236596964*ln_x - 14.135045491064645*log(chi->p1))/chi->p37b9; /* psi3PN_et0_2_Coeff */

}

