/*
 *  Copyright (C) 2022 Phurailatpam Hemantakumar
 *  Assembled from code found in:
 *    - LALSimInspiralTaylorF2Ecc.c
 *    - LALSimInspiralEccentricityFD.c
 */

/*
 * TaylorF2Ecch
 * For 3 harmonics and leading order in et0 and periastron advancement effect
 * no spin
 * no tidal
 */

// #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <lal/LALConstants.h>
#include <lal/LALAtomicDatatypes.h>

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif


// sine cosine values needed for calculatng harmonics.
// s2b stands for sin(2*beta), and ci_2 stands for pow(cos(iota), 2) .
typedef struct
{
    REAL8 ci, c2i, si_2, ci_2;
} sincos_;

// xi is the harmonics dependent amplitude. p and c stands for plus and cross polarization respectively.
typedef struct
{
    COMPLEX16 xi_p, xi_c;
} xi_;

// chi = f/f_min
// chi power values. p stands for power.
// these values are use for calculating the PN coefficients of k (periastron advancement) and psi (fourier phase).
typedef struct
{
    REAL8 p1, p19b9, p25b9, p28b9, p31b9, p34b9, p37b9, p19b18, p2b3, p4b3;
} chi_struct;

////////////////////////////////////
// early declaration of functions //
////////////////////////////////////
// function to calculate the harmonics dependent part of the amplitude, xi
void Xi_PlusCrossHM( size_t cc, REAL8 et, sincos_* sc, COMPLEX16 *xi_p, COMPLEX16 *xi_c); /*function for calculating xi (harmonics dependent amplitude) */
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
void k_psi_PNe0Coeff(REAL8 *ke, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void psi_PNe0CoeffHM( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 f, REAL8 flso ); /* Harmonic indices dependent */

////////////////////////////////////////////////////////////////
// function to find xi : harmonic dependent part of amplitude //
////////////////////////////////////////////////////////////////
void Xi_PlusCrossHM( size_t cc, REAL8 et, sincos_* sc, COMPLEX16 *xi_p, COMPLEX16 *xi_c)
{
    // et : 0PN leading order eccentricity at frequenc f
    // sc : sine cosine functions of inclination angle
    // xi_p : for plus polarization
    // xi_c : for cross polarization
    switch( cc )
    {            

        // l = 1, n = 0
        case 0:
        {   
            *xi_p = et*(sc->si_2);
            *xi_c = 0.;
            break;
        }
        // l = 1, n = -2
        case 1:
        {   
            *xi_p = et*(1.5 + 1.5*sc->ci_2);
            *xi_c = et*(3.*I*sc->ci);
            break;
        }
        // l = 2, n = -2
        case 2:
        {
            *xi_p = - 2. - 2.*sc->ci_2;
            *xi_c = - 4.*I*sc->ci;
            break;
        }
        // l = 3, n = -2
        case 3:
        {
            *xi_p = - et*(4.5 + 4.5*sc->ci_2);
            *xi_c = - et*(9.*I*sc->ci);
            break;
        }

        default:
        {
            *xi_p = 0.*I;
            *xi_c = 0.*I;
            break;
        }
    }
}


///////////////////////////////////////////
// function to calculate PN coefficients //
///////////////////////////////////////////
// needed for k (periastron advancement) and psi (fourier phase).
// Harmonic indices independent
void k_psi_PNe0Coeff(REAL8 *ke, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    // chi : f/f_min
    // symmetric mass ratio
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    
    //////////////////////////
    // k 1PN Coefficients //
    //////////////////////////
    ke[0] = 3.; /* k1PN_et0_0_Coeff */

    ke[1] = (3./chi->p19b9);  /* k1PN_et0_2_Coeff */

    //////////////////////////
    // k 2PN Coefficients //
    //////////////////////////
    ke[2] = 13.5 - 7.*eta;  /* k2PN_et0_0_Coeff */

    ke[3] = 31.31845238095238/chi->p19b9 + 8.431547619047619/chi->p25b9 - (4.083333333333333*eta)/chi->p19b9 - (16.416666666666664*eta)/chi->p25b9;  /* k2PN_et0_2_Coeff */

    ////////////////////////////
    // k 5b2PN Coefficients //
    ////////////////////////////
    ke[4] = 0;  /* k5b2PN_et0_0_Coeff */

    ke[5] = (-49.349184600139665*(-1. + chi->p1))/chi->p28b9;  /* k5b2PN_et0_2_Coeff */

    //////////////////////////
    // k 3PN Coefficients //
    //////////////////////////
    ke[6] = 0.03125*(2160. - 3978.038658666009*eta + 224.*eta_2);  /* k3PN_et0_0_Coeff */

    ke[7] = (9.841899722852105e-7*(-1.193251e6 + 8.9434977e7*chi->p2b3 + 2.85923842e8*chi->p4b3 - 2.2282512e7*eta - 1.85795232e8*chi->p2b3*eta - 3.5172988134238017e8*chi->p4b3*eta + 4.270056e7*eta_2 + 2.2703856e7*chi->p2b3*eta_2 - 3.424512e6*chi->p4b3*eta_2))/chi->p31b9; /* k3PN_et0_2_Coeff */
    
    //////////////////////////
    // psi 0PN Coefficients //
    //////////////////////////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 

    ////////////////////////////
    // psi 3b2PN Coefficients //
    ////////////////////////////
    se[2] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[3] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */

    //////////////////////////
    // psi 1PN Coefficients //
    //////////////////////////
    se[4] = - 3.4193121693121693 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[5] = 4.6174595113029495/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */

    //////////////////////////
    // psi 2PN Coefficients //
    //////////////////////////
    se[6] = - 96.10716450932226 + 43.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[7] = 56.34521097966795/chi->p19b9 + 12.977443249525058/chi->p25b9 + 0.6305695963516759/chi->p31b9 - (11.02099990468567*eta)/chi->p19b9 - (54.287380863896644*eta)/chi->p25b9 + (11.775120739510275*eta)/chi->p31b9 - (36.277272048441404*eta_2)/chi->p19b9 + (56.50271923709424*eta_2)/chi->p25b9 - (22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */

    ////////////////////////////
    // psi 5b2PN Coefficients //
    ////////////////////////////
    se[8] = 60.06010399779534 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[9] = - 210.61264554948588/chi->p19b9 + 141.88004620884573/chi->p25b9 + 75.95595393565333/chi->p28b9 - 42.66622732728404/chi->p34b9 + (216.02141641228727*eta)/chi->p19b9 - (276.2479120677702*eta)/chi->p25b9 - (169.84953027662291*eta)/chi->p28b9 + (211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */

    //////////////////////////
    // psi 3PN Coefficients //
    //////////////////////////
    se[10] = 229.55467539143297 - 4322.017571041981*eta + 63.80497685185184*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[11] = - 61.572441713118685/chi->p19b9 + 158.359109826785/chi->p25b9 + 830.4127436366826/chi->p28b9 - 1.8075594251680198/chi->p31b9 - 327.46225593097415/chi->p37b9 - (843.1840289315528*eta)/chi->p19b9 - (339.30821080736393*eta)/chi->p25b9 - (29.71198627806088*eta)/chi->p31b9 + (28.006148883848002*eta)/chi->p37b9 - (92.88746834092433*eta_2)/chi->p19b9 - (41.648488332329705*eta_2)/chi->p25b9 + (140.16285329998192*eta_2)/chi->p31b9 + (7.068382176074206*eta_2)/chi->p37b9 - (92.75010314857681*eta_3)/chi->p19b9 + (198.5172942650821*eta_3)/chi->p25b9 - (144.6428183515857*eta_3)/chi->p31b9 + (35.5644754417582*eta_3)/chi->p37b9 - (14.135045491064645*ln_chi)/chi->p37b9; /* psi3PN_et0_2_Coeff */

}

///////////////////////////////////////////
// function to calculate PN coefficients //
///////////////////////////////////////////
// needed for k (periastron advancement) and psi (fourier phase).
// Harmonic indices dependent
void psi_PNe0CoeffHM( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 f, REAL8 flso)
{
    // chi : f/f_min
    // ll : harmonic index
    // ln_x : log of PN parameter
    // f : frequency 
    // flso : frequency at last stable orbit
    // symmetric mass ratio
    REAL8 eta_2 = eta*eta;

    //////////////////////////
    // psi 1PN Coefficients //
    //////////////////////////
    se[4] = -(8.333333333333332*nn)/ll; /* psi1PN_et0_0_Coeff */

    se[5] = (10.494186046511627*nn)/(chi->p19b9*ll); /* psi1PN_et0_2_Coeff */

    //////////////////////////
    // psi 2PN Coefficients //
    //////////////////////////
    se[6] = (( - 126.21031746031746 - 10.*eta)*nn)/ll; /* psi2PN_et0_0_Coeff */

    se[7] = ((64.19878487102324/chi->p19b9 + 29.494076458102615/chi->p25b9 + (28.998243751150728*eta)/chi->p19b9 - (57.42651808785529*eta)/chi->p25b9)*nn)/ll; /* psi2PN_et0_2_Coeff */

    ////////////////////////////
    // psi 5b2PN Coefficients //
    ////////////////////////////
    se[8] =   (-100.53096491487338*nn)/ll - (6.960539278786909 + (167.55160819145564*nn)/ll + 22.689280275926283*eta)*(log(f/flso) - log(ll)); /* psi5b2PN_et0_0_Coeff */
    
    se[9] = (( - 331.6374815048508/chi->p19b9 + 172.62650814583742/chi->p28b9)*nn)/ll; /* psi5b2PN_et0_2_Coeff */

    //////////////////////////
    // psi 3PN Coefficients //
    //////////////////////////
    se[10] = -163.04761904761904*ln_x + ((507.8074314216428 - 1013.6970233660937*eta + 19.791666666666664*eta_2)*nn)/ll; /* psi3PN_et0_0_Coeff */

    se[11] = ( - 21.855386904761904/chi->p19b9 + 21.202568236596964/chi->p37b9)*ln_x + ((222.04259821895513/chi->p19b9 + 180.43170390834212/chi->p25b9 - 4.108073898949294/chi->p31b9 - (309.52963269393405*eta)/chi->p19b9 - (269.8099928586847*eta)/chi->p25b9 - (76.71328660124686*eta)/chi->p31b9 + (42.79468040085441*eta_2)/chi->p19b9 - (158.6848338604637*eta_2)/chi->p25b9 + (147.00767567470572*eta_2)/chi->p31b9)*nn)/ll; /* psi3PN_et0_2_Coeff */

}

